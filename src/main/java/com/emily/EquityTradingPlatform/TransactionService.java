package com.emily.EquityTradingPlatform;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class TransactionService {

	private List<Transaction> transactions = new ArrayList<>(Arrays.asList(
			new Transaction(1, 1.0, 100, "Apple", "Buy"), 
			new Transaction(2, 1.5, 300, "Google", "Sell"),
			new Transaction(3, 2.5, 500, "Amazon", "Sell")));
	
	public List<Transaction> getAllTransactions() {
		return transactions;
	}
	
	public Transaction getTransaction(Integer id) {
		return transactions.stream().filter(t -> t.getId().equals(id)).findFirst().get();
	}
	
	public void addTransaction(Transaction transaction) {
		transactions.add(transaction);
	}

	public void updateTransaction(Transaction transaction, Integer id) {
		for(int i=0; i<transactions.size(); i++) {
			Transaction t = transactions.get(i);
			if(t.getId().equals(id)) {
				transactions.set(i, transaction);
			}
		}
	}

	public void deleteTransaction(Integer id) {
		transactions.removeIf(t -> t.getId().equals(id));
	}
}
