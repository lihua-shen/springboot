package com.emily.EquityTradingPlatform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EquityTradingPlatformApplication {

	public static void main(String[] args) {
		SpringApplication.run(EquityTradingPlatformApplication.class, args);
	}
}
